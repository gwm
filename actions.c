/*
 * actions.c
 *
 * Part of gwm, the Gratuitous Window Manager,
 *     by Gary Wong, <gtw@gnu.org>.
 *
 * Copyright (C) 2009  Gary Wong
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of version 3 of the GNU General Public License as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */

#include <config.h>

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#if HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <xcb/xcb.h>

#include "gwm.h"

#include "actions.h"
#include "frame.h"
#include "managed.h"
#include "menu.h"
#include "window-table.h"

static void external_command( char *name, ... ) {

    if( !fork() ) {
       va_list val;
       int n;
       char **args;
    
       va_start( val, name );
       for( n = 0; va_arg( val, char * ); n++ )
           ;
       va_end( val );

       args = alloca( ( n + 2 ) * sizeof *args );

       args[ 0 ] = name;
    
       va_start( val, name );
       n = 1;
       while( ( args[ n++ ] = va_arg( val, char * ) ) )
           ;
       va_end( val );

       setpgid( 0, 0 );
       
       execvp( name, args );

       _exit( 1 );
    }
}

extern void action_raise_lowest( struct gwm_window *window,
				 xcb_generic_event_t *ev,
				 union callback_param cp ) {
    
    xcb_circulate_window( c, XCB_CIRCULATE_RAISE_LOWEST,
			  screens[ window->screen ]->root );
}

extern void action_stack_opposite( struct gwm_window *window,
				   xcb_generic_event_t *ev,
				   union callback_param cp ) {
    
    if( window->type == WINDOW_FRAME ) {
	uint32_t n = XCB_STACK_MODE_OPPOSITE;
	
	xcb_configure_window( c, window->w, XCB_CONFIG_WINDOW_STACK_MODE,
			      &n );
    }
}

extern void action_root_menu( struct gwm_window *window,
			      xcb_generic_event_t *ev,
			      union callback_param cp ) {

    /* FIXME this should be configurable, of course */
    const struct menuitem root_menu[] = {
	{ "Map all icons", action_map_all_icons },
	{ "Raise lowest window", action_raise_lowest },
	{ "xterm", action_start_xterm },
	{ NULL },
	{ "Exit", action_exit }
    };
    
    popup_menu( window, ev, sizeof root_menu / sizeof *root_menu, root_menu );
}

extern void action_iconify_window( struct gwm_window *window,
				   xcb_generic_event_t *ev,
				   union callback_param cp ) {

    if( window->type == WINDOW_FRAME )
	window = window->u.frame.child;
    
    if( window->type == WINDOW_MANAGED &&
	window->u.managed.state == STATE_NORMAL )
	normal_to_iconic( window );
}

extern void action_deiconify_window( struct gwm_window *window,
				     xcb_generic_event_t *ev,
				     union callback_param cp ) {

    if( window->type == WINDOW_FRAME )
	window = window->u.frame.child;
    
    if( window->type == WINDOW_MANAGED &&
	window->u.managed.state == STATE_ICONIC )
	iconic_to_normal( window );
}

extern void action_max_window( struct gwm_window *window,
				xcb_generic_event_t *ev,
				union callback_param cp ) {

    if( window->type == WINDOW_FRAME )
	window = window->u.frame.child;

    if( window->type == WINDOW_MANAGED )
	set_managed_net_state( window,
			       atoms[ ATOM__NET_WM_STATE_MAXIMIZED_HORZ ],
			       NET_STATE_TOGGLE );
}

extern void action_map_raise( struct gwm_window *window,
			      xcb_generic_event_t *ev,
			      union callback_param cp ) {

    if( window->type == WINDOW_FRAME )
	window = window->u.frame.child;
    
    if( window->type == WINDOW_MANAGED ) {
	uint32_t n;

	n = XCB_STACK_MODE_ABOVE;
	xcb_configure_window( c, window->u.managed.frame->w,
			      XCB_CONFIG_WINDOW_STACK_MODE, &n );
	
	if( window->u.managed.state == STATE_ICONIC )
	    iconic_to_normal( window );
    }
}

extern void action_map_all_icons( struct gwm_window *window,
				  xcb_generic_event_t *ev,
				  union callback_param cp ) {
    
    int i;

    for( i = 0; i < windows.used; i++ )
	if( windows.values[ i ]->type == WINDOW_MANAGED &&
	    windows.values[ i ]->u.managed.state == STATE_ICONIC )
	    iconic_to_normal( windows.values[ i ] );
}

extern void action_start_xterm( struct gwm_window *window,
				xcb_generic_event_t *ev,
				union callback_param cp ) {

    external_command( "xterm", NULL );
}

extern void action_window_menu( struct gwm_window *window,
				xcb_generic_event_t *ev,
				union callback_param cp ) {

    /* FIXME this should be configurable, of course */
    const struct menuitem window_menu[] = {
	{ "Iconify", action_iconify_window },
	{ "Raise/lower", action_stack_opposite },
	{ "Zoom", action_max_window }
    };
    
    popup_menu( window, ev, sizeof window_menu / sizeof *window_menu,
		window_menu );
}

static xcb_window_t place_holder;
static int window_was_iconic, window_was_dead, dead_x, dead_y;

static void window_list_activate( struct gwm_window *window,
				  xcb_generic_event_t *ev,
				  union callback_param cp ) {

    if( place_holder ) {
	xcb_destroy_window( c, place_holder );
	place_holder = XCB_NONE;
    }
}

static void window_list_enter( struct gwm_window *window,
			       xcb_generic_event_t *ev,
			       union callback_param cp ) {

    struct gwm_window *client;

    if( ( client = lookup_window( cp.l ) ) ) {
	uint32_t values[ 4 ];
	int live_x, live_y;

	/* Introduce another (unmapped and otherwise unused) window into the
	   stack to mark the original position of the client in the stacking
	   order.  This might seem ugly, but an alternative scheme to
	   allow us to track the stacking order synchronously ourselves
	   would require another round trip to the server, which would be
	   worse. */
	place_holder = xcb_generate_id( c );
	xcb_create_window( c, 0, place_holder, screens[ client->screen ]->root,
			   0, 0, 1, 1, 0, XCB_WINDOW_CLASS_INPUT_ONLY,
			   screens[ client->screen ]->root_visual, 0, NULL );
	values[ 0 ] = client->u.managed.frame->w;
	values[ 1 ] = XCB_STACK_MODE_BELOW;
	xcb_configure_window( c, place_holder, XCB_CONFIG_WINDOW_SIBLING |
			      XCB_CONFIG_WINDOW_STACK_MODE, values );

	if( ( window_was_iconic = client->u.managed.state == STATE_ICONIC ) )
	    iconic_to_normal( client );

	live_x = dead_x = client->u.managed.frame->u.frame.x;
	live_y = dead_y = client->u.managed.frame->u.frame.y;

	make_area_live( client->screen, &live_x, &live_y,
			client->u.managed.frame->u.frame.width,
			client->u.managed.frame->u.frame.height, 8, 8 );

	if( ( window_was_dead = live_x != dead_x || live_y != dead_y ) ) {
	    values[ 0 ] = live_x;
	    values[ 1 ] = live_y;
	    values[ 2 ] = pointer_demux; /* the menu */
	    values[ 3 ] = XCB_STACK_MODE_BELOW;
	} else {
	    values[ 0 ] = pointer_demux; /* the menu */
	    values[ 1 ] = XCB_STACK_MODE_BELOW;
	}

	xcb_configure_window( c, client->u.managed.frame->w,
			      window_was_dead ? XCB_CONFIG_WINDOW_X |
			      XCB_CONFIG_WINDOW_Y |
			      XCB_CONFIG_WINDOW_SIBLING |
			      XCB_CONFIG_WINDOW_STACK_MODE :
			      XCB_CONFIG_WINDOW_SIBLING |
			      XCB_CONFIG_WINDOW_STACK_MODE, values );

	if( focus_frame != client->u.managed.frame ) {
	    deactivate_focus_frame();

	    focus_frame = client->u.managed.frame;

	    activate_focus_frame( event_time( ev ) );
	}
    }
}

static void window_list_leave( struct gwm_window *window,
			       xcb_generic_event_t *ev,
			       union callback_param cp ) {

    struct gwm_window *client;
    
    if( ( client = lookup_window( cp.l ) ) ) {
	uint32_t values[ 4 ];

	if( window_was_dead ) {
	    values[ 0 ] = dead_x;
	    values[ 1 ] = dead_y;
	    values[ 2 ] = place_holder;
	    values[ 3 ] = XCB_STACK_MODE_ABOVE;
	} else {
	    values[ 0 ] = place_holder;
	    values[ 1 ] = XCB_STACK_MODE_ABOVE;
	}
	xcb_configure_window( c, client->u.managed.frame->w,
			      window_was_dead ?
			      XCB_CONFIG_WINDOW_X |
			      XCB_CONFIG_WINDOW_Y |
			      XCB_CONFIG_WINDOW_SIBLING |
			      XCB_CONFIG_WINDOW_STACK_MODE :
			      XCB_CONFIG_WINDOW_SIBLING |
			      XCB_CONFIG_WINDOW_STACK_MODE, values );

	if( window_was_iconic )
	    normal_to_iconic( client );

	if( focus_frame == client->u.managed.frame ) {
	    deactivate_focus_frame();

	    xcb_set_input_focus( c, XCB_INPUT_FOCUS_NONE,
				 XCB_INPUT_FOCUS_POINTER_ROOT,
				 event_time( ev ) );

	    focus_frame = NULL;
	}
    }

    if( place_holder ) {
	xcb_destroy_window( c, place_holder );
	place_holder = XCB_NONE;
    }
}

extern void action_window_list_menu( struct gwm_window *window,
				     xcb_generic_event_t *ev,
				     union callback_param cp ) {

    int i, num_items;
    
    struct menuitem *items;

    for( i = 0, num_items = 0; i < num_screens; i++ ) {
	xcb_window_t w = screens[ i ]->root | STACK_END;

	if( i )
	    num_items++;

	do {
	    struct gwm_window *window = lookup_window( w );
	    xcb_window_t next =
		stack_lookup( &window_stack, w )->lower_window;

	    if( window && window->type == WINDOW_FRAME )
		num_items++;

	    w = next;
	} while( w != ( screens[ i ]->root | STACK_END ) );
    }
    
    items = alloca( num_items * sizeof *items );

    for( i = 0, num_items = 0; i < num_screens; i++ ) {
	xcb_window_t w = screens[ i ]->root | STACK_END;

	if( i ) {
	    items[ num_items ].label = NULL;

	    num_items++;
	}

	do {
	    struct gwm_window *window = lookup_window( w );
	    xcb_window_t next =
		stack_lookup( &window_stack, w )->lower_window;

	    if( window && window->type == WINDOW_FRAME ) {
		struct gwm_window *managed = window->u.frame.child;
		char *name = managed->u.managed.name ?
		    managed->u.managed.name : "(Untitled)";

		if( managed->u.managed.state == STATE_ICONIC ) {
		    int len = strlen( name );
		    char *new = alloca( len + 3 );

		    new[ 0 ] = '[';
		    strcpy( new + 1, name );
		    new[ len + 1 ] = ']';
		    new[ len + 2 ] = 0;

		    name = new;
		}

		items[ num_items ].label = name;
		items[ num_items ].action = window_list_activate;
		items[ num_items ].enter_action = window_list_enter;
		items[ num_items ].leave_action = window_list_leave;
		items[ num_items ].cp.l = managed->w;
		items[ num_items ].icon = managed->w;

		num_items++;
	    }

	    w = next;
	} while( w != ( screens[ i ]->root | STACK_END ) );
    }

    popup_menu( window, ev, num_items, items );
}

extern void action_exit( struct gwm_window *window, xcb_generic_event_t *ev,
			 union callback_param cp ) {

    /* FIXME prompt for confirmation of exit */
    
    signal_caught = -1;
}

/* FIXME make the current frame bindings (move, resize, close?) actions too */
