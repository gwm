#ifndef MENU_H
#define MENU_H

struct menuitem {
    const char *label;
    void ( *action )( struct gwm_window *window, xcb_generic_event_t *ev,
		      union callback_param cp );
    void ( *enter_action )( struct gwm_window *window, xcb_generic_event_t *ev,
			    union callback_param cp );
    void ( *leave_action )( struct gwm_window *window, xcb_generic_event_t *ev,
			    union callback_param cp );
    union callback_param cp;
    xcb_window_t icon;
};
extern void popup_menu( struct gwm_window *window, xcb_generic_event_t *ev,
			int num_items, const struct menuitem *items );

extern const event_handler menu_handlers[], menuitem_handlers[];

#endif
