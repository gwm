/*
 * root.c
 *
 * Part of gwm, the Gratuitous Window Manager,
 *     by Gary Wong, <gtw@gnu.org>.
 *
 * Copyright (C) 2009  Gary Wong
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of version 3 of the GNU General Public License as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id$
 */

#include <config.h>

#if USE_RANDR
#include <xcb/randr.h>
#endif
#include <xcb/xcb.h>

#include "gwm.h"

#include "actions.h"
#include "frame.h"
#include "keyboard.h"
#include "managed.h"
#include "root.h"
#include "window-table.h"

static void root_key_press( struct gwm_window *window,
			    xcb_key_press_event_t *ev ) {

    int i;

    for( i = 0; i < num_key_actions; i++ )
	if( keyboard_map[ ev->detail ][ 0 ] == key_actions[ i ].keysym &&
	    ( ev->state & 0xFF & key_actions[ i ].modifiers ) ==
	    key_actions[ i ].modifiers ) {
	    union callback_param cp;

	    cp.p = NULL;
	    key_actions[ i ].handler( focus_frame ? focus_frame : window,
				      (xcb_generic_event_t *) ev, cp );

	    break;
	}
}

static const struct button_action {
    int button;
    xcb_mod_mask_t modifiers;
    void ( *handler )( struct gwm_window *window, xcb_generic_event_t *ev,
		       union callback_param cp );
} button_actions[] = {
    /* FIXME This table should be configurable, of course. */
    { 1, 0, action_root_menu },
    { 2, 0, action_window_list_menu },
    { 3, 0, action_start_xterm }
};

#define NUM_BUTTON_ACTIONS ( sizeof button_actions / sizeof *button_actions )

static void root_button_press( struct gwm_window *window,
                              xcb_button_press_event_t *ev ) {

    int i;
    
    if( ev->child )
       return;

    for( i = 0; i < NUM_BUTTON_ACTIONS; i++ )
	if( ev->detail == button_actions[ i ].button &&
	    ( ev->state & 0xFF & button_actions[ i ].modifiers ) ==
	    button_actions[ i ].modifiers ) {
	    union callback_param cp;

	    cp.p = NULL;
	    button_actions[ i ].handler( window, (xcb_generic_event_t *) ev,
					 cp );

	    break;
	}
}

static void root_enter_notify( struct gwm_window *window,
			       xcb_enter_notify_event_t *ev ) {
    
    if( focus_frame && ( ev->detail == XCB_NOTIFY_DETAIL_INFERIOR ||
			 ev->detail == XCB_NOTIFY_DETAIL_NONLINEAR ) ) {
	deactivate_focus_frame();
	
	xcb_set_input_focus( c, XCB_INPUT_FOCUS_NONE,
			     XCB_INPUT_FOCUS_POINTER_ROOT, ev->time );

	focus_frame = NULL;
    }

    install_window_colormap( window->screen, NULL, ev->time );
}

static void root_create_notify( struct gwm_window *window,
				xcb_create_notify_event_t *ev ) {

    /* We only ever update the window stack in response to notify
       events from the server.  The drawback to this approach is that
       our stack can become slightly stale (recently transmitted requests
       might not be reflected in our state).  However, this policy has
       the critical advantage that the window stack is always internally
       consistent: if we updated it eagerly instead, that would introduce
       the possibility of non-causal ordering of events. */
    stack_insert_above( &window_stack, ev->window,
			stack_lookup( &window_stack,
				      window->w | STACK_END )->lower_window );
}

static void root_destroy_notify( struct gwm_window *window,
				 xcb_destroy_notify_event_t *ev ) {

    stack_remove( &window_stack, ev->window );
}

static void root_reparent_notify( struct gwm_window *window,
				  xcb_reparent_notify_event_t *ev ) {

    if( ev->parent == window->w ) {
	/* It's possible to reparent a window to its current parent,
	   so we can't assume the window is not already in the stack. */
	if( stack_lookup( &window_stack, ev->window ) )
	    stack_move_above( &window_stack, ev->window,
			      stack_lookup( &window_stack, window->w |
					    STACK_END )->lower_window );
	else
	    stack_insert_above( &window_stack, ev->window,
				stack_lookup( &window_stack, window->w |
					      STACK_END )->lower_window );
    } else
	stack_remove( &window_stack, ev->window );
}

static void root_configure_notify( struct gwm_window *window,
				   xcb_configure_notify_event_t *ev ) {

    if( ev->event == ev->window ) {
	/* The root window itself was configured. */
	if( screens[ window->screen ]->width_in_pixels !=
	    ev->width ||
	    screens[ window->screen ]->height_in_pixels !=
	    ev->height ) {
	    uint32_t values[ 4 ];

	    screens[ window->screen ]->width_in_pixels = ev->width;
	    screens[ window->screen ]->height_in_pixels = ev->height;

	    values[ 0 ] = ev->width;
	    values[ 1 ] = ev->height;
	    xcb_change_property( c, XCB_PROP_MODE_REPLACE, ev->window,
				 atoms[ ATOM__NET_DESKTOP_GEOMETRY ], CARDINAL,
				 32, 2, values );

	    values[ 0 ] = 0;
	    values[ 1 ] = 0;
	    values[ 2 ] = ev->width;
	    values[ 3 ] = ev->height;
	    xcb_change_property( c, XCB_PROP_MODE_REPLACE, ev->window,
				 atoms[ ATOM__NET_WORKAREA ], CARDINAL, 32, 4,
				 values );
	}
    } else
	/* A child of the root was configured. */
	stack_move_above( &window_stack, ev->window, ev->above_sibling ?
			  ev->above_sibling : window->w | STACK_END );
}

extern void root_circulate_request( struct gwm_window *window,
				    xcb_circulate_request_event_t *ev ) {

    uint32_t value = ev->place == XCB_PLACE_ON_TOP ? XCB_STACK_MODE_ABOVE :
	XCB_STACK_MODE_BELOW;

    /* Circulating children of the root -- honour the request as is. */
    xcb_configure_window( c, ev->window, XCB_CONFIG_WINDOW_STACK_MODE,
			  &value );
}

static void root_synthetic( struct gwm_window *root,
			    xcb_generic_event_t *ev ) {

    xcb_unmap_notify_event_t *unmap = (xcb_unmap_notify_event_t *) ev;
    xcb_configure_request_event_t *config =
	(xcb_configure_request_event_t *) ev;
    struct gwm_window *window;
	
    switch( ev->response_type & ~SEND_EVENT_MASK ) {
    case XCB_UNMAP_NOTIFY:
	/* Handle a synthetic UnmapNotify request to move a window to
	   the Withdrawn state (see ICCCM 2.0, section 4.1.4). */
	if( ( window = lookup_window( unmap->window ) ) &&
	    window->type == WINDOW_MANAGED )
	    unmanage_window( window );

	break;
	
    case XCB_CONFIGURE_REQUEST:
	/* Handle a synthetic ConfigureRequest, which should be used
	   only to restack managed windows relative to windows which
	   were originally siblings (see ICCCM 4.1.5). */
	if( ( config->value_mask & XCB_CONFIG_WINDOW_STACK_MODE ) &&
	    ( window = lookup_window( config->window ) ) &&
	    window->type == WINDOW_MANAGED ) {
	    struct gwm_window *sibling;
	    uint32_t values[ 2 ];

	    if( ( sibling = lookup_window( config->sibling ) ) &&
		sibling->type == WINDOW_MANAGED )
		sibling = sibling->u.managed.frame;

	    values[ 0 ] = sibling ? sibling->w : config->sibling;
	    values[ 1 ] = config->stack_mode;
	    
	    handle_error_reply( xcb_configure_window_checked(
				    c, window->u.managed.frame->w,
				    XCB_CONFIG_WINDOW_SIBLING |
				    XCB_CONFIG_WINDOW_STACK_MODE,
				    values ),
				ERR_MASK_VALUE | ERR_MASK_WINDOW |
				ERR_MASK_MATCH );
	}
	
	break;
    }
}

#if USE_RANDR
static void root_rr_crtc_change_notify( struct gwm_window *window,
					xcb_randr_notify_event_t *ev ) {

    int rotated = ev->u.cc.rotation & ( XCB_RANDR_ROTATION_ROTATE_90 |
					XCB_RANDR_ROTATION_ROTATE_270 );

    update_crtc( window->screen, ev->u.cc.crtc,
		 rotated ? ev->u.cc.height : ev->u.cc.width,
		 rotated ? ev->u.cc.width : ev->u.cc.height,
		 ev->u.cc.x, ev->u.cc.y );
}
#endif

const event_handler root_handlers[ NUM_EXTENDED_EVENTS ] = {
    NULL, /* Error */
    NULL, /* Reply */
    (event_handler) root_key_press,
    NULL, /* KeyRelease */
    (event_handler) root_button_press,
    NULL, /* ButtonRelease */
    NULL, /* MotionNotify */
    (event_handler) root_enter_notify,
    NULL, /* LeaveNotify */
    NULL, /* FocusIn */
    NULL, /* FocusOut */
    NULL, /* KeymapNotify */
    NULL, /* Expose */
    NULL, /* GraphicsExpose */
    NULL, /* NoExposure */
    NULL, /* VisibilityNotify */
    (event_handler) root_create_notify,
    (event_handler) root_destroy_notify,
    NULL, /* UnmapNotify */
    NULL, /* MapNotify */
    (event_handler) withdrawn_map_request,
    (event_handler) root_reparent_notify,
    (event_handler) root_configure_notify,
    (event_handler) withdrawn_configure_request,
    NULL, /* GravityNotify */
    NULL, /* ResizeRequest */
    NULL, /* CirculateNotify */
    (event_handler) root_circulate_request,
    NULL, /* PropertyNotify */
    NULL, /* SelectionClear */
    NULL, /* SelectionRequest */
    NULL, /* SelectionNotify */
    NULL, /* ColormapNotify */
    NULL, /* ClientMessage */
    NULL, /* MappingNotify */
    (event_handler) root_synthetic,
#if USE_RANDR
    (event_handler) root_rr_crtc_change_notify, /* RRCrtcChangeNotify */
#else
    NULL, /* RRCrtcChangeNotify */
#endif
    NULL  /* ShapeNotify */
};
