#ifndef IMAGE_H
#define IMAGE_H

/* Like xcb_put_image (with XCB_IMAGE_FORMAT_Z_PIXMAP), but capable of
   breaking the data into multiple requests if it's too big for the
   server to handle in one. */
extern xcb_void_cookie_t put_image( xcb_drawable_t drawable, xcb_gcontext_t gc,
				    uint16_t width, uint16_t height, int16_t x,
				    int16_t y, uint8_t left_pad, uint8_t depth,
				    uint32_t len, const uint8_t *data );

#endif
