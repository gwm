#ifndef ACTIONS_H
#define ACTIONS_H

extern void action_raise_lowest( struct gwm_window *window,
				 xcb_generic_event_t *ev,
				 union callback_param cp );

extern void action_stack_opposite( struct gwm_window *window,
				   xcb_generic_event_t *ev,
				   union callback_param cp );

extern void action_root_menu( struct gwm_window *window,
			      xcb_generic_event_t *ev,
			      union callback_param cp );

extern void action_iconify_window( struct gwm_window *window,
				   xcb_generic_event_t *ev,
				   union callback_param cp );

extern void action_deiconify_window( struct gwm_window *window,
				     xcb_generic_event_t *ev,
				     union callback_param cp );

extern void action_max_window( struct gwm_window *window,
				     xcb_generic_event_t *ev,
				     union callback_param cp );

extern void action_map_raise( struct gwm_window *window,
			      xcb_generic_event_t *ev,
			      union callback_param cp );

extern void action_map_all_icons( struct gwm_window *window,
				  xcb_generic_event_t *ev,
				  union callback_param cp );

extern void action_start_xterm( struct gwm_window *window,
				xcb_generic_event_t *ev,
				union callback_param cp );

extern void action_window_menu( struct gwm_window *window,
				xcb_generic_event_t *ev,
				union callback_param cp );

extern void action_window_list_menu( struct gwm_window *window,
				     xcb_generic_event_t *ev,
				     union callback_param cp );

extern void action_exit( struct gwm_window *window, xcb_generic_event_t *ev,
			 union callback_param cp );

#endif
    
