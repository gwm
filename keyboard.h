#ifndef KEYBOARD_H
#define KEYBOARD_H

/* Map of keysyms: first index is keycode, second index is group/modifier. */
extern xcb_keysym_t keyboard_map[ 0x100 ][ 4 ];
/* Map of modifiers, indexed by xcb_map_index_t. */
extern xcb_keycode_t *modifier_map[ 8 ];

extern const struct key_action {
    xcb_keysym_t keysym;
    xcb_mod_mask_t modifiers;
    void ( *handler )( struct gwm_window *window, xcb_generic_event_t *ev,
		       union callback_param cp );
} key_actions[];
extern const int num_key_actions;

extern void get_keyboard_mapping( int first, int count );
extern void get_modifier_mapping( void );
#if DEBUG
extern void cleanup_keyboard( void );
#endif

#endif
