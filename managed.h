#ifndef MANAGED_H
#define MANAGED_H

extern void iconic_to_normal( struct gwm_window *window );
extern void normal_to_iconic( struct gwm_window *window );

extern void set_managed_state( struct gwm_window *window, int state );

#define NET_STATE_REMOVE 0
#define NET_STATE_ADD 1
#define NET_STATE_TOGGLE 2

extern void set_managed_net_state( struct gwm_window *window,
				   xcb_atom_t state, int action );

#if USE_SHAPE
extern void match_managed_shape( struct gwm_window *window );
#endif

extern void managed_property_change( struct gwm_window *window, int prop,
				     xcb_get_property_reply_t *p );
extern void async_get_property( struct gwm_window *window,
				enum gwm_property_type prop );

extern const event_handler managed_handlers[];

extern void withdrawn_map_request( struct gwm_window *window,
				   xcb_map_request_event_t *ev );
extern void withdrawn_configure_request( struct gwm_window *window,
					 xcb_configure_request_event_t *ev );

#endif
