#ifndef _GWM_H_
#define _GWM_H_

#ifndef FALSE
#define FALSE 0
#endif
#ifndef TRUE
#define TRUE 1
#endif

#if !defined(__GNUC__) && !defined(__attribute__)
#define __attribute__()
#endif

#ifndef CONST
#define CONST __attribute__((const))
#endif

#ifndef FORMAT
#define FORMAT( archetype, string_index, first_to_check ) \
    __attribute__((format( archetype, string_index, first_to_check ) ))
#endif

#ifndef MALLOC
#define MALLOC __attribute__((malloc))
#endif

#ifndef NORETURN
#define NORETURN __attribute__((noreturn))
#endif

#ifndef PURE
#define PURE __attribute__((pure))
#endif

#if HAVE_INIT_SECTION
#define INIT __attribute__((section( ".gwminit.text" ) ))
#define INITD __attribute__((section( ".gwminit.rodata" ) ))
#else
#define INIT
#define INITD
#endif

#define SEND_EVENT_MASK 0x80

extern MALLOC void *xmalloc( size_t size );
extern void *xrealloc( void *p, size_t size );
extern MALLOC void *xcalloc( size_t number, size_t size );

enum _extended_event_type {
    SYNTHETIC_EVENT = XCB_MAPPING_NOTIFY + 1,
    RANDR_CRTC_CHANGE_NOTIFY,
    SHAPE_NOTIFY,
    NUM_EXTENDED_EVENTS
};

extern xcb_connection_t *c;
extern xcb_timestamp_t latest_timestamp;

enum x_atom {
    /* See X Window System Protocol (version 11, release 6.7), Appendix B. */
    ATOM = 4,
    CARDINAL = 6,
    INTEGER = 19,
    RGB_COLOR_MAP = 24,
    RGB_DEFAULT_MAP = 27,
    STRING = 31,
    WINDOW = 33,
    WM_HINTS = 35,
    WM_ICON_NAME = 37,
    WM_ICON_SIZE = 38,
    WM_NAME = 39,
    WM_NORMAL_HINTS = 40,
    WM_SIZE_HINTS = 41
};

enum gwm_atom {
    ATOM_COMPOUND_TEXT,
    ATOM_MANAGER,
    ATOM__MOTIF_WM_HINTS,
    ATOM__NET_CURRENT_DESKTOP,
    ATOM__NET_DESKTOP_GEOMETRY,
    ATOM__NET_DESKTOP_VIEWPORT,
    ATOM__NET_FRAME_EXTENTS,
    ATOM__NET_NUMBER_OF_DESKTOPS,
    ATOM__NET_SUPPORTED,
    ATOM__NET_SUPPORTING_WM_CHECK,
    ATOM__NET_WM_ICON,
    ATOM__NET_WM_NAME,
    ATOM__NET_WM_STATE,
    ATOM__NET_WM_STATE_FULLSCREEN,
    ATOM__NET_WM_STATE_MAXIMIZED_HORZ,
    ATOM__NET_WM_STATE_MAXIMIZED_VERT,
    ATOM__NET_WORKAREA,
    ATOM_UTF8_STRING,
    ATOM_VERSION,
    ATOM_WM_CHANGE_STATE,
    ATOM_WM_COLORMAP_NOTIFY,
    ATOM_WM_COLORMAP_WINDOWS,
    ATOM_WM_DELETE_WINDOW,
    ATOM_WM_PROTOCOLS,
    ATOM_WM_STATE,
    ATOM_WM_TAKE_FOCUS,
    NUM_ATOMS
};

extern xcb_atom_t atoms[ NUM_ATOMS ];

enum gwm_property_type {
    PROP__MOTIF_WM_HINTS,
    PROP__NET_WM_ICON,
    PROP__NET_WM_NAME,
    PROP__NET_WM_STATE,
    PROP_WM_COLORMAP_WINDOWS,
    PROP_WM_HINTS,
    PROP_WM_NAME,
    PROP_WM_NORMAL_HINTS,
    PROP_WM_PROTOCOLS,
    NUM_PROPS
};

#define PROP_SIZE 262144 /* maximum number of 32-bit words to retrieve */

extern xcb_atom_t prop_atoms[ NUM_PROPS ];
extern xcb_atom_t prop_types[ NUM_PROPS ];

enum gwm_extension {
#if USE_COMPOSITE
    EXT_COMPOSITE,
#endif
#if USE_DAMAGE
    EXT_DAMAGE,
#endif
#if USE_RANDR
    EXT_RANDR,
#endif
#if USE_RENDER
    EXT_RENDER,
#endif
#if USE_SHAPE
    EXT_SHAPE, /* any version */
#endif
#if USE_XFIXES
    EXT_XFIXES, /* requires version 2 */
#endif
    NUM_EXTENSIONS
};

#if USE_COMPOSITE || USE_DAMAGE || USE_RANDR || USE_RENDER || USE_SHAPE || \
    USE_XFIXES
#define EXTENSIONS_SIZE NUM_EXTENSIONS
#else
#define EXTENSIONS_SIZE 1 /* avoid zero length arrays */
#endif

extern int have_extension[ EXTENSIONS_SIZE ];
extern uint8_t extension_event[ EXTENSIONS_SIZE ],
    extension_error[ EXTENSIONS_SIZE ];

enum decoration_col {
    COL_FRAME_ACTIVE,
    COL_FRAME_INACTIVE,
    COL_BORDER,
    COL_BUTTON_ACTIVE,
    COL_BUTTON_INACTIVE,
    COL_TITLE_ACTIVE,
    COL_TITLE_INACTIVE,
    COL_FEEDBACK_BACK,
    COL_FEEDBACK_FORE,
    COL_MENU_ACTIVE_BACK,
    COL_MENU_ACTIVE_FORE,
    COL_MENU_INACTIVE_BACK,
    COL_MENU_INACTIVE_FORE,
    NUM_COLS
};

extern int num_screens;
extern xcb_screen_t **screens;

#if USE_RANDR
struct gwm_crtc {
    uint32_t crtc;
    int x, y, width, height;
};
#endif

struct gwm_screen {
    xcb_visualtype_t *root_visual;
    xcb_atom_t wm_atom; /* the atom WM_Sn, where n is the screen number */
    xcb_colormap_t cmap; /* the most recently installed colormap */
    xcb_timestamp_t cmap_time; /* the time at which it was installed */
    uint32_t pixels[ NUM_COLS ]; /* pixel values in the default cmap */
#if USE_RANDR
    struct gwm_crtc **crtcs;
    int num_crtcs;
#endif
};

extern struct gwm_screen *gwm_screens;

enum border_region {
    BR_TL,
    BR_T,
    BR_TR,
    BR_L,
    BR_R,
    BR_BL,
    BR_B,
    BR_BR,
    NUM_BORDER_REGIONS
};

enum gwm_cursor {
    CURSOR_TL,
    CURSOR_T,
    CURSOR_TR,
    CURSOR_L,
    CURSOR_C,
    CURSOR_R,
    CURSOR_BL,
    CURSOR_B,
    CURSOR_BR,
    CURSOR_ARROW,
    CURSOR_DESTROY,
    NUM_CURSORS
};
/* FIXME these should really be indexed by (screen,cursor), not just by
   cursor -- screens might be different resolutions, for instance. */
extern xcb_cursor_t cursors[ NUM_CURSORS ];

union callback_param {
    long l;
    void *p;
};

struct gwm_window;

#define HINT_ICONIC 0x1 /* otherwise normal */
#define HINT_INPUT 0x2
#define HINT_USER_POSITION 0x4
#define HINT_PROGRAM_POSITION 0x8
#define HINT_POSITION 0xC /* HINT_USER_POSITION | HINT_PROGRAM_POSITION */
#define PROTOCOL_DELETE_WINDOW 0x1
#define PROTOCOL_TAKE_FOCUS 0x2

#define STATE_WITHDRAWN 0
#define STATE_NORMAL 1
#define STATE_ICONIC 3

#define DEC_BORDER 0x1
#define DEC_TITLE 0x2
#define DEC_DEFAULT ( DEC_BORDER | DEC_TITLE )

#define ZOOM_NORMAL 0x0
#define ZOOM_FULLSCREEN 0x1 /* _NET_WM_STATE_FULLSCREEN */
#define ZOOM_MAXIMISE 0x2 /* _NET_WM_STATE_MAXIMIZED_* */

struct gwm_window {
    xcb_window_t w;
    int screen;
    enum _gwm_window_type {
	WINDOW_ROOT, WINDOW_MANAGED, WINDOW_FRAME, WINDOW_BUTTON, WINDOW_MENU,
	WINDOW_MENUITEM, WINDOW_FAKE, WINDOW_FEEDBACK, WINDOW_INCOMPLETE,
	WINDOW_CHILDLESS
    } type;
    xcb_rectangle_t update; /* the area which needs updating */
    int cleared; /* TRUE if the update area has already been cleared */
    union _gwm_window_union {
	struct _gwm_managed {
	    struct gwm_window *frame;
	    uint16_t border_width;
	    xcb_colormap_t cmap;
#if USE_SHAPE
	    int shaped;
#endif
	    /* from WM_COLORMAP_WINDOWS: */
	    xcb_window_t cmap_window;
	    /* from WM_HINTS: */
	    int hints; /* see HINT_* above */
	    /* from _NET_WM_ICON: */
#if USE_RENDER
	    uint32_t *full_icons, *menu_icons; /* pictures, screen indexed */
	    int net_wm_icon;
#endif
	    /* from WM_NAME and _NET_WM_NAME: */
	    char *name; /* legal UTF-8; must be free()d */
	    int net_wm_name;
	    /* from WM_NORMAL_HINTS: */
	    int min_width, min_height, max_width, max_height, width_inc,
		height_inc, min_aspect_x, min_aspect_y, max_aspect_x,
		max_aspect_y, base_width, base_height, win_gravity;
	    /* from WM_PROTOCOLS: */
	    int protocols; /* see PROTOCOL_* above */
	    /* WM_STATE: */
	    int state; /* see STATE_* above */
	} managed;
	struct _gwm_frame {
	    struct gwm_window *child, *button;
	    int x, y, width, height;
	    int decoration; /* see DEC_* above */
	    int unzoomed_x, unzoomed_y, unzoomed_width, unzoomed_height,
		unzoomed_decoration;
	    int zoom_state; /* see ZOOM_* above */
	    xcb_window_t border_regions[ NUM_BORDER_REGIONS ];
	} frame;
	struct _gwm_button {
	    struct gwm_window *frame;
	} button;
	struct _gwm_menu {
	    xcb_window_t window_param;
	    int num_items, active_item;
	    int width;
	    int has_icons;
	    struct gwm_window **items; /* must be free()d */
	} menu;
	struct _gwm_menuitem {
	    struct gwm_window *menu;
	    char *label; /* must be free()d */
	    void ( *action )( struct gwm_window *window,
			      xcb_generic_event_t *ev,
			      union callback_param cp );
	    void ( *enter_action )( struct gwm_window *window,
				    xcb_generic_event_t *ev,
				    union callback_param cp );
	    void ( *leave_action )( struct gwm_window *window,
				    xcb_generic_event_t *ev,
				    union callback_param cp );
	    union callback_param cp;
	    xcb_window_t icon;
	} menuitem;
	struct _gwm_fake {
	    xcb_timestamp_t timestamp;	    
	} fake;
	struct _gwm_feedback {
	    int fb_width, fb_height;
	} feedback;
	struct _gwm_incomplete {
	    unsigned int sequence;
	} incomplete;
    } u;
};

extern struct gwm_window *fake_window; /* a window created solely to hold the
					  WM_Sn selections */

extern const char *argv0;
extern volatile int signal_caught;

extern FORMAT( printf, 1, 2 ) void warning( char *format, ... );
extern FORMAT( printf, 1, 2 ) NORETURN void fatal( char *format, ... );
    
/* Schedule a callback function to be invoked when a reply or error is
   available for handling, or when neither can arrive. */
extern void handle_async_reply( unsigned int sequence,
				void ( *callback )( unsigned int sequence,
						    void *reply,
						    xcb_generic_error_t *error,
						    union callback_param p ),
				union callback_param p );
/* Block until the scheduled callback function has completed. */
extern void sync_with_callback( unsigned int sequence );

#define ERR_MASK_REQUEST 0x000001
#define ERR_MASK_VALUE 0x00002
#define ERR_MASK_WINDOW 0x00004
#define ERR_MASK_PIXMAP 0x00008
#define ERR_MASK_ATOM 0x00010
#define ERR_MASK_CURSOR 0x00020
#define ERR_MASK_FONT 0x00040
#define ERR_MASK_MATCH 0x00080
#define ERR_MASK_DRAWABLE 0x00100
#define ERR_MASK_ACCESS 0x00200
#define ERR_MASK_ALLOC 0x00400
#define ERR_MASK_COLORMAP 0x00800
#define ERR_MASK_G_CONTEXT 0x01000
#define ERR_MASK_ID_CHOICE 0x02000
#define ERR_MASK_NAME 0x04000
#define ERR_MASK_LENGTH 0x08000
#define ERR_MASK_IMPLEMENTATION 0x10000

extern void handle_error_reply( xcb_void_cookie_t cookie, unsigned ignore );
extern void show_error( xcb_generic_error_t *error );

typedef void ( *event_handler )( struct gwm_window *window,
				 xcb_generic_event_t *ev );

/* Return TRUE iff an event is the only button pressed. */
extern CONST int initial_press( xcb_button_press_event_t *ev );

/* Return TRUE iff an event is the release of the last button (and so would
   terminate a passive grab). */
extern CONST int final_release( xcb_button_release_event_t *ev );

/* Return TRUE iff a point is "live" (within at least one CRTC). */
extern int point_live( int screen, int x, int y );

/* Move an area the shortest Manhattan distance so that at an area of at
   least min_x x min_y pixels fall within at least one CRTC, if possible. */
extern void make_area_live( int screen, int *x, int *y, int width, int height,
			    int min_x, int min_y );

extern CONST xcb_timestamp_t event_time( xcb_generic_event_t *ev );

/* The window whose event handler(s) should be invoked for pointer events.
   Unlike pointer grabs, this mechanism is implemented entirely on the
   client side, and has no effect on the contents of events. */
extern xcb_window_t pointer_demux;

/* Install a window's colormap on a screen, or the default colormap if
   unspecified. */
extern void install_window_colormap( int screen, struct gwm_window *window,
				     xcb_timestamp_t t );

extern void update_crtc( int screen, int crtc, int width, int height,
			 int x, int y );

extern void generic_expose( struct gwm_window *window,
			    xcb_expose_event_t *ev );

extern void manage_window( xcb_window_t w, int map_request );
extern void unmanage_window( struct gwm_window *window );

/* A replacement for xcb_wait_for_event (which is broken, because it
   always restarts interrupted system calls).  Also invokes any
   asynchronous callbacks scheduled for replies and errors. */
extern xcb_generic_event_t *wait_for_event( void );

extern void ( *window_size )( struct gwm_window *window, int *width,
			      int *height );
extern void ( *replace_icons )( struct gwm_window *window, int num_icons,
				int *widths, int *heights, uint32_t **icons,
				xcb_pixmap_t *pixmaps );

#endif
