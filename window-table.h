#ifndef WINDOW_TABLE_H
#define WINDOW_TABLE_H

extern struct window_table {
    struct half_table {
	xcb_window_t key;
	int index;
    } *t[ 2 ];
    struct gwm_window **values; /* compact array containing the values, to
				   make iteration more efficient */
    int n; /* number of entries in each half table */
    int used; /* number of entries occupied */
    int max; /* maximum chain length to follow during insertion */
    unsigned long hash;
} windows, update_windows;

extern void table_init( struct window_table *table );
#if DEBUG
extern void table_destroy( struct window_table *table );
#endif
extern PURE struct gwm_window *table_lookup( struct window_table *table,
					     xcb_window_t key );
extern void queue_window_update( struct gwm_window *window,
				 int x, int y, int width, int height,
				 int cleared );
extern void window_update_done( struct gwm_window *window );
extern MALLOC struct gwm_window *add_window( xcb_window_t w );
extern void forget_window( struct gwm_window *window );
extern struct gwm_window *lookup_window( xcb_window_t w );

#define STACK_END 0x20000000
#define STACK_MASK 0xE0000000

extern struct window_stack {
    struct stacking_order {
	xcb_window_t window, lower_window, higher_window;
    } *t[ 2 ];
    int n; /* number of entries in each half table */
    int used; /* number of entries occupied */
    int max; /* maximum chain length to follow during insertion */
    unsigned long hash;
} window_stack;

extern void stack_init( struct window_stack *stack );
#if DEBUG
extern void stack_destroy( struct window_stack *stack );
#endif
extern PURE struct stacking_order *stack_lookup( struct window_stack *stack,
						 xcb_window_t key );
extern void stack_insert_singleton( struct window_stack *stack,
				    xcb_window_t key );
extern void stack_insert_above( struct window_stack *stack,
				xcb_window_t key, xcb_window_t lower_window );
extern void stack_remove( struct window_stack *stack, xcb_window_t window );
extern void stack_move_above( struct window_stack *stack,
			      xcb_window_t key, xcb_window_t lower_window );
extern xcb_window_t stack_real_below( struct window_stack *stack,
				      xcb_window_t window );
#endif
