#ifndef ROOT_H
#define ROOT_H

extern void root_circulate_request( struct gwm_window *window,
				    xcb_circulate_request_event_t *ev );

extern const event_handler root_handlers[];

#endif
